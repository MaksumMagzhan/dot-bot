import React, { Component } from "react";
import { getTemplates, addTemplate, getFolders } from "../../services/template";

import "./Templates.scss";

class Templates extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: "Не классифицированные",
      templates: [],
      folders: [
        {
          id: 0,
          title: "Не классифицированные"
        }
      ]
    };

    this.addTemplate = this.addTemplate.bind(this);
  }

  componentDidMount() {
    getTemplates().then(response => {
      this.setState({
        templates: response.data
      });
    });
    getFolders().then(response => {
      response.data.forEach(item => {
        this.setState({
          folders: [...this.state.folders, item]
        });
      });
    });
  }

  addTemplate() {
    const text = document.getElementById("text").value;
    addTemplate(text).then(response => {
      this.setState({
        templates: [...this.state.templates, text]
      });
    });
    document.getElementById("text").value = "";
  }

  render() {
    return (
      <div className="Templates main">
        <div className="Templates__table">
          <h3 className="Templates__header">Существующие шаблоны</h3>
          <table>
            <tr>
              <th>№</th>
              <th>Группа</th>
            </tr>
            {this.state.folders.map((folder, index) => {
              return (
                <tr>
                  <td>{index}</td>
                  <td>{folder.title ? folder.title : ""}</td>
                </tr>
              );
            })}
          </table>
        </div>
        <div className="Templates__table">
          <h3 className="Templates__header">{this.state.value}</h3>
          <table>
            <tr>
              <th>№</th>
              <th>Текс шаблона</th>
            </tr>
            {this.state.templates.map((template, index) => {
              return (
                <tr>
                  <td>{index}</td>
                  <td>{template.text ? template.text : template}</td>
                </tr>
              );
            })}
          </table>
        </div>
        <div className="Templates__add">
          <h3 className="Templates__header">Добавление шаблона</h3>
          <div className="Templates__form" onSubmit={this.addTemplate}>
            <div className="form-group">
              <label htmlFor="text" className="form-group__label">
                Введите текст шаблона
              </label>
              <input
                id="text"
                className="form-group__input"
                type="text"
              ></input>
            </div>
            <button className="button" onClick={this.addTemplate}>
              Добавить +
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default Templates;
