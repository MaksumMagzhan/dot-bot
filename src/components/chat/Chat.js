import React, { Component } from "react";

import { getChats, getResolvedChats, resolveChat } from "../../services/chat";

import Dashboard from "./dashboard/Dashboard";
import Messenger from "./messenger/Messenger";

import { UserIdContext } from '../../context';

import "./Chat.scss";
import MessengerWelcome from "./messenger-welcome/MessengerWelcome";


class Chat extends Component {
  state = {
    chats: [],
    finishedChats: [],
    user: '',
  };

  componentDidMount() {
    this.getChats();
    this.interval = setInterval(() => this.getChats(), 10000);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  getChats = () => {
    getChats().then(response => {
      const newElements = [];
      response.data.forEach(item => {
        newElements.push(item);
      });
      this.setState({
        chats: newElements
      })
    });

    getResolvedChats().then(response => {
      const newElements = [];
      response.data.forEach(item => {
        newElements.push(item);
      });
      this.setState({
        finishedChats: newElements
      })
    });
  }

  onDashboardClick = id => {
    this.setState({
      user: id
    });
  }

  onResolveChat = (pk, comment, type) => {
    resolveChat(pk, comment, type).then(response => {
      this.getChats();
    })
  }

  render() {
    return (
      <div className="Chat main">
        <UserIdContext.Provider value={this.state.user}>
          <Dashboard onDashboardClick={this.onDashboardClick} user={this.state.user} onResolveChat={this.onResolveChat} chats={this.state.chats} finishedChats={this.state.finishedChats}></Dashboard>
          {this.state.user ? <UserIdContext.Consumer>{user => <Messenger user={user}></Messenger>}</UserIdContext.Consumer> : <MessengerWelcome></MessengerWelcome>}
        </UserIdContext.Provider>
      </div>
    );
  }
}

export default Chat;
