import React, { Component } from "react";

import { getChat, sendMessage } from "../../../services/chat";
import { getTemplates } from "../../../services/template";

import sendIcon from "../../../assets/images/send.png";

import "./Messenger.scss";

class Messenger extends Component {
  state = {
    templateValue: "",
    messages: [],
    templates: [],
    resolved: false
  };

  chatRef = React.createRef();
  templateRef = React.createRef();

  componentDidMount() {
    this.getChatContent(this.props.user);
    getTemplates().then(response => {
      this.setState({
        templates: response.data
      });
    });
    this.interval = setInterval(
      () => this.getChatContent(this.props.user),
      10000
    );
  }

  componentDidUpdate(prevProps) {
    if (this.props.user !== prevProps.user) {
      this.getChatContent(this.props.user);
    }
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  getChatContent(id) {
    getChat(Number(id)).then(response => {
      this.setState({
        messages: response.data.messages,
        resolved: response.data.is_resolved
      });
      const chat = document.getElementById("chat");
      chat.scrollTop = chat.scrollHeight;
    });
  }

  enterPressed(event) {
    var code = event.keyCode || event.which;
    if (code === 13) {
      this.onSendMessage();
    }
  }

  onTemplateValueClick = event => {
    this.setState({
      templateValue: event.target.innerText
    });
    document.getElementById("message").value = event.target.innerText;
    this.showTemplates();
  };

  showTemplates() {
    if (document.getElementById("templates").style.display !== "block") {
      document.getElementById("templates").style.display = "block";
    } else {
      document.getElementById("templates").style.display = "none";
    }
  }

  onSendMessage = () => {
    const input = document.getElementById("message");
    const index = this.state.messages.length;
    const now = new Date();
    var date =
      now.getFullYear() + "-" + (now.getMonth() + 1) + "-" + now.getDate();
    var time = now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds();
    const dateTime = date + " " + time;
    const inputObejct = {
      chat: this.state.messages[0].chat,
      from_who: "system",
      datetime_created: dateTime,
      id: index,
      options: [],
      text: input.value
    };
    this.setState({
      messages: [...this.state.messages, inputObejct]
    });

    sendMessage(Number(this.props.user), input.value).then(response => {});
    document.getElementById("message").value = "";
    if (this.chatRef && this.chatRef.current) {
      setTimeout(() => {
        this.chatRef.current.scrollTop = this.chatRef.current.scrollHeight;
      }, 0);
    }
  };

  render() {
    return (
      <div className="Messenger">
        <div className="Messenger__header">
          {this.state.messages.length &&
            this.state.messages[0].datetime_created.substring(0, 10)}
        </div>
        <div className="Messenger__chat" id="chat" ref={this.chatRef}>
          {this.state.messages.map((message, index) => {
            return (
              <div
                className={`message__wrapper message__wrapper--${
                  message.from_who === "system" ? "right" : "left"
                }`}
              >
                {index !== 0 &&
                  message.datetime_created.substring(0, 10) !==
                    this.state.messages[index - 1].datetime_created.substring(
                      0,
                      10
                    ) && (
                    <p className="message__date">
                      {message.datetime_created.substring(0, 10)}
                    </p>
                  )}
                {index === 0 && (
                  <p className="message__author">
                    {message.datetime_created.substring(11, 16)}
                  </p>
                )}
                {index !== 0 &&
                  message.from_who !==
                    this.state.messages[index - 1].from_who && (
                    <p className="message__author">
                      {message.datetime_created.substring(11, 16)}
                    </p>
                  )}
                <p
                  className={`chat__message chat__message--${
                    message.from_who === "system" ? "right" : "left"
                  } chat__message--${
                    index !== 0 &&
                    message.from_who === this.state.messages[index - 1].from_who
                      ? "next"
                      : "first"
                  }`}
                >
                  {message.text}
                </p>
              </div>
            );
          })}
        </div>
        {this.state.resolved ? (
          <div className="Messenger__resolved">Сеанс был завершен</div>
        ) : (
          <div className="Messenger__input">
            <React.Fragment>
              <button className="input__template" onClick={this.showTemplates}>
                Выберите шаблон
              </button>
              <div
                id="templates"
                className="input__template-list"
                ref={this.templateRef}
              >
                {this.state.templates.map(template => {
                  return (
                    <div
                      className="template-list__element"
                      onClick={this.onTemplateValueClick}
                    >
                      {template.text}
                    </div>
                  );
                })}
              </div>
              <input
                id="message"
                type="text"
                className="input__text"
                placeholder="Введите сообщение..."
                onKeyPress={this.enterPressed.bind(this)}
              ></input>
              <button className="input__send" onClick={this.onSendMessage}>
                <img src={sendIcon} alt="send" width="20px"></img>
              </button>
            </React.Fragment>
          </div>
        )}
      </div>
    );
  }
}

export default Messenger;
