import React, { Component } from "react";

import DashboardItem from "./dashboard-item/DashboardItem";

import "./Dashboard.scss";

class Dashboard extends Component {
  state = {
    user: ""
  };

  onDashboardClick = id => {
    this.props.onDashboardClick(id);
  };

  onResolveChat = (pk, comment, type) => {
    this.props.onResolveChat(pk, comment, type);
  };

  render() {
    return (
      <div className="Dashboard">
        {!this.props.finished ? (
          <div className="Dashboard__current">
            <div className="Dashboard__wait">
              Ожидание ({this.props.chats.length})
            </div>
            {this.props.chats.map(chat => (
              <DashboardItem
                active={chat.id === this.props.user}
                new={chat.is_new_message}
                id={chat.id}
                person={chat.client}
                status={chat.messenger_title}
                onResolveChat={this.onResolveChat}
                onClick={() => this.onDashboardClick(chat.id)}
              ></DashboardItem>
            ))}
          </div>
        ) : (
          <div className="Dashboard__history">
            <div className="Dashboard__wait">
              Завершенные ({this.props.finishedChats.length})
            </div>
            <div className="Dashboard__list">
              {this.props.finishedChats.map(chat => (
                <DashboardItem
                  id={chat.id}
                  person={chat.client}
                  status={chat.messenger_title}
                  done
                  onClick={() => this.onDashboardClick(chat.id)}
                ></DashboardItem>
              ))}
            </div>
          </div>
        )}
      </div>
    );
  }
}

export default Dashboard;
