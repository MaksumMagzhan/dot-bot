import React, { Component } from "react";

import { getUser } from "../../../../services/users";

import messageIcon from "../../../../assets/images/message.png";
import telegramIcon from "../../../../assets/images/telegram.png";
import wppIcon from "../../../../assets/images/wpp.png";
import "./DashboardItem.scss";
import { getResolves } from "../../../../services/chat";

class DashboardItem extends Component {
  state = {
    username: "",
    chat: "",
    resolve: "",
    resolved: false,
    resolves: [],
    active: false,
    new: this.props.new
  };

  componentDidMount() {
    getUser(this.props.person).then(response => {
      this.setState({
        username: `${response.data.first_name} ${response.data.last_name}`,
        chat: response.data.type
      });
    });

    getResolves().then(response => {
      const newEl = [];
      response.data.forEach(resolve => {
        const obj = {
          value: resolve[0],
          title: resolve[1]
        };
        newEl.push(obj);
      });
      this.setState({
        resolves: newEl
      });
    });
  }

  handleChange = event => {
    this.setState({
      resolve: event.target.value
    });
  };

  handleResolve = () => {
    this.props.onResolveChat(this.props.id, "", this.state.resolve);
  };

  handleClick = () => {
    this.props.onClick();
    this.setState({
      active: true
    });
  };

  render() {
    return (
      <div
        className={
          "DashboardItem " +
          (this.props.active
            ? "DashboardItem--active"
            : this.state.new
            ? "DashboardItem--new"
            : "")
        }
        onClick={this.handleClick}
      >
        <div className="DashboardItem__img">
          <img
            src={
              this.state.chat === "telegram"
                ? telegramIcon
                : this.state.chat === "watsapp"
                ? wppIcon
                : messageIcon
            }
            width="50px"
            alt="logo"
          ></img>
        </div>
        <div className="DashboardItem__main">
          <p className="main__main">{this.state.username}</p>
          <p className="main__sec">{this.props.status}</p>
        </div>
        <div className="DashboardItem__descr">
          {!this.props.done ? (
            <div>
              <select onChange={this.handleChange}>
                <option disabled selected>
                  Как все прошло?
                </option>
                {this.state.resolves.length &&
                  this.state.resolves.map(resolve => (
                    <option value={resolve.value}>{resolve.title}</option>
                  ))}
              </select>
              <button className="resolve" onClick={this.handleResolve}>
                Завершить
              </button>
            </div>
          ) : (
            <span>Завершен</span>
          )}
        </div>
      </div>
    );
  }
}

export default DashboardItem;
