import React, { Component } from 'react';

import './MessengerWelcome.scss';

class MessengerWelcome extends Component {
  render() {
    return (
      <div className="MessengerWelcome">
        <h1>Выберите чат, чтобы начать общение!</h1>
      </div>
    );
  }
}

export default MessengerWelcome;