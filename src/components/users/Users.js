import React, { Component } from "react";

import UsersTable from "./users-table/UsersTable";

import "./Users.scss";

class Users extends Component {
  render() {
    return (
      <div className="Users main">
        <UsersTable></UsersTable>
      </div>
    );
  }
}

export default Users;
