import React, { Component } from 'react';
import { getTemplates } from '../../services/template';

class Widget extends Component {

  componentDidMount() {
    getTemplates().then(response => console.log(response));
  }

  render() {
    return (
      <div className="Widget main">

      </div>
    );
  }
}

export default Widget;