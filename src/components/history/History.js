import React, { Component } from "react";

import { getReport } from "../../services/chat";
import { getUser } from "../../services/users";

class History extends Component {
  state = {
    chats: [],
    users: []
  };

  componentDidMount() {
    this.getChats();
  }

  getChats = () => {
    getReport().then(response => {
      const clients = [];
      const newElements = [];
      response.data.forEach(item => {
        newElements.push(item);
        const user = {
          id: item.id,
          name: ""
        };
        getUser(item.id).then(response => {
          user.name = `${response.data.first_name} ${response.data.last_name}`;
          clients.push(user);
        });
      });
      this.setState({
        chats: newElements,
        users: clients
      });
    });
  };

  setUserName = () => {
    this.state.users.map(user => {
      this.state.chats.map(chat => {
        const newChat = chat;
        if (chat.id === user.id) {
          newChat.id = user.name;
          this.setState({
            chats: [...newChat]
          });
        }
      });
    });
  };

  render() {
    return (
      <div className="History main">
        <div className="Templates__table">
          <h3 className="Templates__header">История чатов</h3>
          <table>
            <tr>
              <th>Имя</th>
              <th>Номер телефона</th>
              <th>Ресурс</th>
              <th>Оператор</th>
              <th>Время инициации чата</th>
              <th>Время завершения чата</th>
              <th>Длительность</th>
              <th>Результат</th>
            </tr>
            {this.state.chats.map((item, index) => {
              return (
                <tr>
                  <td>{item.id ? item.id : ""}</td>
                  <td>{item.phone ? item.phone : ""}</td>
                  <td>{item.source ? item.source : ""}</td>
                  <td>{item.operator ? item.operator : ""}</td>
                  <td>
                    {item.datetime_accepted
                      ? `${item.datetime_accepted.substring(
                          0,
                          10
                        )}, ${item.datetime_accepted.substring(11, 16)}`
                      : ""}
                  </td>
                  <td>
                    {item.datetime_closed
                      ? `${item.datetime_closed.substring(
                          0,
                          10
                        )}, ${item.datetime_closed.substring(11, 16)}`
                      : ""}
                  </td>
                  <td>{item.time_of_waiting ? item.time_of_waiting : ""}</td>
                  <td>{item.status ? item.status : ""}</td>
                </tr>
              );
            })}
          </table>
        </div>
      </div>
    );
  }
}

export default History;
