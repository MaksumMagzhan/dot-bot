import React from "react";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";

import "./App.css";
import Sidebar from "./sidebar/Sidebar";
import Users from "./components/users/Users";
import Chat from "./components/chat/Chat";
import Widget from "./components/widget/Widget";
import Templates from "./components/templates/Templates";
import History from "./components/history/History";

function App() {
  return (
    <div className="App">
      {/* <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header> */}
      <BrowserRouter>
        <Sidebar></Sidebar>
        <Switch>
          <Route path="/users" component={Users}></Route>
          <Route path="/chat" component={Chat}></Route>
          <Route path="/templates" component={Templates}></Route>
          <Route path="/history" component={History}></Route>
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
