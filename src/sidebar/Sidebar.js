import React, { Component } from "react";
import { NavLink } from "react-router-dom";

import routes from "./routes";
import "./Sidebar.scss";

class Sidebar extends Component {
  render() {
    return (
      <aside className="Sidebar">
        {routes.map(route => {
          return (
            <NavLink
              key={route.id}
              to={route.path}
              className="Sidebar__item"
              activeClassName="Sidebar__item--active"
            >
              <div className={"Sidebar__icon " + route.iconClass} />
              <div className="Sidebar__text">{route.text}</div>
            </NavLink>
          );
        })}
      </aside>
    );
  }
}

export default Sidebar;
