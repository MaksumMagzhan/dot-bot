import axios from "axios";

const baseURL = "http://127.0.0.1:8000";

const instance = axios.create({
  baseURL
});

if (localStorage.getItem("admin-token")) {
  instance.defaults.headers.common["admin-token"] = localStorage.getItem(
    "admin-token"
  );
}

instance.defaults.headers.common["token"] = localStorage.getItem("token");

export default instance;
