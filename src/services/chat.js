import api from "./api";

export function getChats() {
  const headers = {
    Authorization: "token a7b56e9bb3f0afb9c2536c216aa2dafb898903bd"
  };
  return api.get("/agents/chat/", { headers });
}

export function getChat(pk) {
  const headers = {
    Authorization: "token a7b56e9bb3f0afb9c2536c216aa2dafb898903bd"
  };
  return api.get(`/agents/chat/${pk}`, { headers });
}

export function sendMessage(pk, message) {
  const headers = {
    Authorization: "token a7b56e9bb3f0afb9c2536c216aa2dafb898903bd",
    "Content-Type": "application/x-www-form-urlencoded"
  };
  const value = `text=${message}`;

  return api.post(`/agents/chat/${pk}/send_message/`, value, { headers });
}

export function getResolves() {
  const headers = {
    Authorization: "token a7b56e9bb3f0afb9c2536c216aa2dafb898903bd"
  };
  return api.get(`/agents/chat/resolve_types/`, { headers });
}

export function getResolvedChats() {
  const headers = {
    Authorization: "token a7b56e9bb3f0afb9c2536c216aa2dafb898903bd"
  };
  return api.get(`/agents/chat/resolved_chats/`, { headers });
}

export function resolveChat(pk, comment, type) {
  const headers = {
    Authorization: "token a7b56e9bb3f0afb9c2536c216aa2dafb898903bd",
    "Content-Type": "application/x-www-form-urlencoded"
  };
  const value = `comment=${comment}&type=${type}`;

  return api.post(`/agents/chat/${pk}/resolve/`, value, { headers });
}

export function getReport() {
  const headers = {
    Authorization: "token a7b56e9bb3f0afb9c2536c216aa2dafb898903bd"
  };
  return api.get(`/agents/report/`, { headers });
}
