import api from "./api";

export function getUser(id) {
  const headers = {
    Authorization: "token a7b56e9bb3f0afb9c2536c216aa2dafb898903bd"
  };
  return api.get(`/agents/client/${id}`, { headers });
}