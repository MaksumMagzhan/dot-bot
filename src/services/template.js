import api from "./api";

export function getTemplates() {
  const headers = {
    Authorization: "token a7b56e9bb3f0afb9c2536c216aa2dafb898903bd"
  };

  return api.get("/agents/option/", { headers });
}

export function addTemplate(text) {
  const headers = {
    Authorization: "token a7b56e9bb3f0afb9c2536c216aa2dafb898903bd",
    "Content-Type": "application/x-www-form-urlencoded"
  };

  const value = `text=${text}`;

  return api.post("/agents/option/", value, { headers });
}

export function getFolders() {
  const headers = {
    Authorization: "token a7b56e9bb3f0afb9c2536c216aa2dafb898903bd"
  };

  return api.get("/agents/option_folder/", { headers });
}
